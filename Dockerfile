FROM node:latest
WORKDIR /app
COPY package.json /app
COPY package-lock.json /app
RUN npm install
EXPOSE 5200
COPY . /app
CMD node customer.js
