'use strict';
const requests = require('requests');
const parseString = require('xml2js').parseString;
const Excel = require('exceljs')
const fs = require('fs');
const json2xls = require('json2xls');
const PDFParser = require("pdf2json");
const node_xj = require("xls-to-json");
const parser = require('simple-excel-to-json');
const dateFormat = require("dateformat");
const csv=require('csvtojson');

var controller = {

  about: function (req, res) {
    var aboutInfo = {
      name: "properties.name",
      version: "properties.version"
    }
    res.send(aboutInfo);
  },

  getConsoildateList: function (req, res) {


    // // Country Mapping Lis
    var countryList = parser.parseXls2Json('country_list_mapping.xlsx');
    //
    // // Calling Api NACTA Individual
    var jsonArr = [];
    requests('https://nfs.punjab.gov.pk/Home/GetXML?filter=', { streaming: false })
      .on('data', function (chunk) {
        // console.log(chunk);
        var data = ""
        //Converting XML to JSON
        parseString(chunk, function (err, result) {

          var objNactaInd = JSON.parse(JSON.stringify(result));
          var arr = objNactaInd.ArrayOfHomeModel.HomeModel;

          for (let i = 0; i < arr.length; i++) {
            // console.log(number(i+1));
            var json = {
              "Customer ID": number(i+1),
              "First Name": replaceNACTA(arr[i].Name[0],true),
              "Middle Name": '',
              "Last Name": replaceNACTA(arr[i].FatherName[0],true),
              "Date of Birth":'',
              "Nationality":'Pakistan',
              "Gender":'NACTA Ind',
              "Country of Residence":'Pakistan',
              "ID Type":'National ID Card',
              "ID Number":replaceNACTA(arr[i].CNIC[0])
            };

            jsonArr.push(json);

          }

        });

        //Read Excel for NACTA Entities
        var nactaEntities = parser.parseXls2Json('NactaEntities.xlsx');
        // console.log(nactaEntities[0]);
        var objNactaEntities = JSON.parse(JSON.stringify(nactaEntities[0]));
        var arrNactaEntities = objNactaEntities;

        for (let i = 0; i < arrNactaEntities.length; i++) {
          // console.log(arrNactaEntities[i].Name_of_Organization);
          var json = {
            "Customer ID": number(jsonArr.length+1),
            "First Name": replaceNACTA(arrNactaEntities[i].Name_of_Organization,false),
            "Middle Name": '',
            "Last Name": '',
            "Date of Birth":'',
            "Nationality":'Pakistan',
            "Gender":'NACTA Ent',
            "Country of Residence":'Pakistan',
            "ID Type":'',
            "ID Number":''
          };

          jsonArr.push(json);

        }

      })
      .on('end', function (err) {
        if (err) return console.log('connection closed due to errors', err);
        console.log('nacta-end');

        getUNList();
      });

    function getUNList(){

      // Calling  UN 
      requests('https://scsanctions.un.org/resources/xml/en/consolidated.xml', { streaming: false })
        .on('data', function (chunk) {
          // console.log(chunk);
          var data = ""
          //Converting XML to JSON
          parseString(chunk, function (err, result) {

            // var objNactaInd = JSON.parse(JSON.stringify(result));
            // console.log(JSON.stringify(result));

            var objUN = JSON.parse(JSON.stringify(result));
            var arrInd = objUN.CONSOLIDATED_LIST.INDIVIDUALS[0].INDIVIDUAL;
            var arrEntity = objUN.CONSOLIDATED_LIST.ENTITIES[0].ENTITY;


            //UN for Individuals
            for (let i = 0; i < arrInd.length; i++) {


              let fName = 'FIRST_NAME' in arrInd[i] ? arrInd[i].FIRST_NAME[0] : ''
              let sName = 'SECOND_NAME' in arrInd[i] ? arrInd[i].SECOND_NAME[0] : ''
              let tName = 'THIRD_NAME' in arrInd[i] ? arrInd[i].THIRD_NAME[0] : ''
              let foName = 'FOURTH_NAME' in arrInd[i] ? arrInd[i].FOURTH_NAME[0] : ''
              let name = replaceUN(fName.trim()+" "+sName.trim()+" "+tName.trim()+" "+foName.trim(),true);
              name = name.replace(/^"(.*)"$/, '$1');
              name = name.replace(/['"]+/g, '')
              name = name.replace(/[^a-zA-Z ]/g, "")
              name = name.replace(/[`"]+/g, '')

              let DOB = formatDate('DATE' in arrInd[i].INDIVIDUAL_DATE_OF_BIRTH[0] ? arrInd[i].INDIVIDUAL_DATE_OF_BIRTH[0].DATE[0] : 'YEAR' in arrInd[i].INDIVIDUAL_DATE_OF_BIRTH[0] ? arrInd[i].INDIVIDUAL_DATE_OF_BIRTH[0].YEAR[0] : '',"dd/mm/yyyy");
              
              // let nationality = 'NATIONALITY' in arrInd[i] ? arrInd[i].NATIONALITY[0].VALUE[0] : '';

              // if(nationality){
              //   nationality = replaceUN(nationality,false);
              //   nationality = countryMapping(countryList[0],nationality,true);
              // }

              let countryOfRes =''
              if(arrInd[i].INDIVIDUAL_ADDRESS[0].COUNTRY[0]){
                countryOfRes = replaceUN(arrInd[i].INDIVIDUAL_ADDRESS[0].COUNTRY[0],true);
                countryOfRes = countryOfRes.replace('-0-','');
                countryOfRes = countryMapping(countryList[0],countryOfRes,true);
              }

              let docType = arrInd[i].INDIVIDUAL_DOCUMENT[0].hasOwnProperty('TYPE_OF_DOCUMENT') ? arrInd[i].INDIVIDUAL_DOCUMENT[0].TYPE_OF_DOCUMENT[0] : '';
              let idType = '';
              let docId ='';
              if(docType == 'Passport' || docType == 'National Identification Number'){
                idType = arrInd[i].INDIVIDUAL_DOCUMENT[0].hasOwnProperty('TYPE_OF_DOCUMENT') ? arrInd[i].INDIVIDUAL_DOCUMENT[0].TYPE_OF_DOCUMENT[0] : '';
                docId = arrInd[i].INDIVIDUAL_DOCUMENT[0].hasOwnProperty('NUMBER') ? arrInd[i].INDIVIDUAL_DOCUMENT[0].NUMBER[0] : '';
              }

              idType = idType.replace(/National Identification Number/ig,'National ID Card');
              idType = idType.replace(/national identity card number/ig,'National ID Card');
              idType = idType.replace(/Afghan national identification card/ig,'National ID Card');
              idType = idType.replace(/French national identity card number/ig,'National ID Card');


          

              let nationality = 'NATIONALITY' in arrInd[i] //? arrInd[i].NATIONALITY[0].VALUE[0] : '';
              
              console.log('exists',nationality)


              if(nationality){

                  console.log('lenght',arrInd[i].NATIONALITY[0].VALUE.length)
                

                  for(let n=0; n < arrInd[i].NATIONALITY[0].VALUE.length; n++){
                    
                    nationality = replaceUN(arrInd[i].NATIONALITY[0].VALUE[n],false);
                    nationality = countryMapping(countryList[0],nationality,true);

                    if(nationality.trim() == 'na'){
                      nationality = ''
                    }

                    var json = {
                      "Customer ID": number(jsonArr.length+1),
                      "First Name": name,
                      "Middle Name": '',
                      "Last Name": '',
                      "Date of Birth":DOB,
                      "Nationality":nationality,
                      "Gender":'UN Ind',
                      "Country of Residence":countryOfRes,
                      "ID Type":idType,
                      // "ID Number":replaceUN(docId.replace(/-/g,''),true)
                      "ID Number":replacePassport(docId.replace(/-/g,''),true)
                    };
                    jsonArr.push(json);
                  }
                  
               
              }else{

                 // For Names
                var json = {
                  "Customer ID": number(jsonArr.length+1),
                  "First Name": name,
                  "Middle Name": '',
                  "Last Name": '',
                  "Date of Birth":DOB,
                  "Nationality":'',
                  "Gender":'UN Ind',
                  "Country of Residence":countryOfRes,
                  "ID Type":idType,
                  // "ID Number":replaceUN(docId.replace(/-/g,''),true)
                  "ID Number":replacePassport(docId.replace(/-/g,''),true)
                };
                jsonArr.push(json);
              }

             

              //For Alias
              if(arrInd[i].hasOwnProperty('INDIVIDUAL_ALIAS')){
                for(let l=0; l < arrInd[i].INDIVIDUAL_ALIAS.length; l++){
                  let alias = arrInd[i].INDIVIDUAL_ALIAS[l].ALIAS_NAME[0];
                  alias = alias.replace(/^"(.*)"$/, '$1');
                  alias = alias.replace(/['"]+/g, '')
                  alias = alias.replace(/[`"]+/g, '')
                  let quality = arrInd[i].INDIVIDUAL_ALIAS[l].QUALITY[0];

                  if(quality == 'Good'){
                    if(alias){

                      if(nationality == false){
                        nationality = ''
                      }


                      var jsonAlias = {
                        "Customer ID": number(jsonArr.length+1),
                        "First Name": replaceUN(alias,true),
                        "Middle Name": '',
                        "Last Name": '',
                        "Date of Birth":DOB,
                        "Nationality":nationality,
                        "Gender":'UN Ind',
                        "Country of Residence":countryOfRes,
                        "ID Type":idType,
                        // "ID Number":replaceUN(docId.replace(/-/g,''),true)
                        "ID Number":replacePassport(docId.replace(/-/g,''),true)
                      };
                      jsonArr.push(jsonAlias);
                    }
                  }
                }

              }
            }

            //UN For Entities
            for (let i = 0; i < arrEntity.length; i++) {
              // console.log(i);
              let entFName = 'FIRST_NAME' in arrEntity[i] ? arrEntity[i].FIRST_NAME[0] : ''
              
              let entCountryOfRes = ''
              
              if(arrEntity[i].ENTITY_ADDRESS[0].hasOwnProperty('COUNTRY')){
                entCountryOfRes = arrEntity[i].ENTITY_ADDRESS[0].hasOwnProperty('COUNTRY') ? arrEntity[i].ENTITY_ADDRESS[0].COUNTRY[0] : '';              
                entCountryOfRes = replaceUN(entCountryOfRes,false);
                entCountryOfRes = entCountryOfRes.replace('-0-','');
                entCountryOfRes = countryMapping(countryList[0],entCountryOfRes,true);
              }

              var json = {
                "Customer ID": number(jsonArr.length+1),
                "First Name": replaceUN(entFName,false),
                "Middle Name": '',
                "Last Name": '',
                "Date of Birth":'',
                "Nationality":'',
                "Gender":'UN ENT',
                "Country of Residence": entCountryOfRes,
                "ID Type":'',
                "ID Number":''
              };
              jsonArr.push(json);

              // For Alias
              if(arrEntity[i].hasOwnProperty('ENTITY_ALIAS')){
                for(let k=0; k < arrEntity[i].ENTITY_ALIAS.length; k++){
                  let alias = arrEntity[i].ENTITY_ALIAS[k].ALIAS_NAME[0];
                  let quality = arrEntity[i].ENTITY_ALIAS[k].QUALITY[0];
                  if(quality == 'Good'){
                    if(alias){
                      // console.log('alias Ent:'+replaceUN(alias.replace(/([^\u0621-\u063A\u0641-\u064A\u0660-\u0669a-zA-Z 0-9])/g, ''),false));
                      var jsonAlias = {
                        "Customer ID": number(jsonArr.length+1),
                        "First Name": replaceUN(alias,false),
                        "Middle Name": '',
                        "Last Name": '',
                        "Date of Birth":'',
                        "Nationality":'',
                        "Gender":'UN ENT',
                        "Country of Residence":entCountryOfRes,
                        "ID Type":'',
                        "ID Number":''
                      };
                      jsonArr.push(jsonAlias);
                    }
                  }
                }
              }
            }

          });

        })
        .on('end', function (err) {
          if (err) return console.log('connection closed due to errors', err);
          console.log('un-end');
          getOFAC();
          // excel(jsonArr);
          res.send("done");
          console.log('done')

        });
    }
    // getOFAC();
    function getOFAC(){
      var jsonArrMain = [];
      var jsonArrAdd =  [];
      var jsonArrAlt =  [];

      var nactaEntities = parser.parseXls2Json('sdnMain.xlsx');
      // console.log(nactaEntities);
      jsonArrAdd = parser.parseXls2Json('add.xlsx');
      // console.log(jsonArrAdd);
      jsonArrAlt = parser.parseXls2Json('alt.xlsx');
      // console.log(jsonArrAlt);   

      for(let a=0; a < nactaEntities[0].length; a++){

        if(nactaEntities[0][a].SDN_Type === 'vessel' || nactaEntities[0][a].SDN_Type === 'aircraft' ){

        }else{
          // console.log(nactaEntities[0][a].SDN_Type);
          jsonArrMain.push(nactaEntities[0][a]);
        }
      }

      for (let i = 0; i < jsonArrMain.length; i++) {
        var countryOfResd ='';
        // // For Getting Address
        // for (let j = 0; j < jsonArrAdd[0].length; j++) {

        //   if(jsonArrAdd[0][j].ID == jsonArrMain[i].ID){
        //     countryOfResd=jsonArrAdd[0][j].country;
        //     break;
        //   }
        // }

        let DOBOFAC ='';
        let genderOFAC ='';
        let nationalityOFAC = '';
        let passportOFAC = '';
        let nationalIDOFAC = '';
        // Getting values from general column
        let words = jsonArrMain[i].general.split(';');

        for(let i=0; i< words.length;i++){
          // console.log('length',words.length)
          if(words[i].includes("DOB")){
              // console.log(words[i].replace("DOB","").trim());
              // DOBOFAC = words[i].replace("DOB","").trim().replace(/alt/ig,"").trim().replace(/ /g, '-');
              DOBOFAC = words[i].split('to')[0];
              DOBOFAC = DOBOFAC.replace("DOB","").trim().replace(/alt/ig,"").trim().replace(/ /g, '-');
              DOBOFAC = DOBOFAC.replace(/circa/ig,"").trim();
              DOBOFAC = DOBOFAC.split('(')[0]

              if(/\d/.test(DOBOFAC)){
                DOBOFAC = formatDate(DOBOFAC,"dd/mm/yyyy")
              }else{
                DOBOFAC =''
              }
              
              // console.log('Date',DOBOFAC);
          }else if(words[i].includes("Gender")){
            // console.log(words[i].replace("Gender","").trim());
            genderOFAC = words[i].replace("Gender","").trim();
          }else if(words[i].includes("nationality")){
            // console.log(words[i].replace("nationality","").trim());
            nationalityOFAC = words[i].replace("nationality","").trim();
            console.log('nationa 1: ',nationalityOFAC)
            nationalityOFAC = replaceUN(nationalityOFAC,true).replace(/alt /ig,"").trim();
            nationalityOFAC = replaceUN(nationalityOFAC,true).trim();
            console.log('nationa 2: ',nationalityOFAC)
          }else if(words[i].includes("Passport")){
            // console.log(words[i].replace("Passport","").trim());
            passportOFAC = words[i].replace("Passport","").trim().replace(/alt/ig,"").trim();
            passportOFAC = passportOFAC.split('issued')[0];
            // passportOFAC = replaceUN(passportOFAC,true);
            passportOFAC = replacePassport(passportOFAC,true)

          }else if(words[i].includes("National ID No")){
              // console.log(words[i].replace("National ID No","").trim());
            nationalIDOFAC = words[i].replace("National ID No","").trim().replace(/alt/ig,"").trim();
            nationalIDOFAC = nationalIDOFAC.split('issued')[0];
            nationalIDOFAC = replaceUN(nationalIDOFAC,true);
          }
        }

        // // For Getting Address
        for (let j = 0; j < jsonArrAdd[0].length; j++) {
          // console.log("yes "+jsonArrAdd[0][j].ID);
          if(jsonArrAdd[0][j].ID == jsonArrMain[i].ID){
            // console.log("no "+jsonArrAdd[0][j].ID);
            countryOfResd=jsonArrAdd[0][j].country;
          
             // For Names
            var json = {
              "Customer ID":number(jsonArr.length+1) ,
              "First Name":replaceUN(jsonArrMain[i].Name.replace(/^"(.*)"$/, '$1').replace(/['"]+/g, '').replace(/[^a-zA-Z ]/g, "") ,true),
              "Middle Name":'',
              "Last Name": '',
              "Date of Birth":DOBOFAC,
              "Nationality":countryMapping(countryList[0], replaceUN(nationalityOFAC,true),false),
              "Gender":'OFAC',
              "Country of Residence":countryMapping(countryList[0], replaceUN(nationalityOFAC,true),false),
              "ID Type":nationalIDOFAC ? 'National ID Card' : passportOFAC? 'Passport':'',
              "ID Number":nationalIDOFAC ? nationalIDOFAC : passportOFAC ? passportOFAC :''
            };
            jsonArr.push(json);
          }
        }


        // Adding alias
        for (let j = 0; j < jsonArrAlt[0].length; j++) {
          var altName='';
          if(jsonArrAlt[0][j].ID == jsonArrMain[i].ID){
            altName =jsonArrAlt[0][j].alt_name;
            altName = altName.replace(/^"(.*)"$/, '$1')
            altName = altName.replace(/['"]+/g, '')
            altName = altName.replace(/[^a-zA-Z ]/g, "")
            // console.log('Alt-Name',altName +" ID:"+jsonArrMain[i].ID )
            var jsonAlias = {
              "Customer ID":number(jsonArr.length+1),
              "First Name":replaceUN(altName ,true),
              "Middle Name":'',
              "Last Name": '',
              "Date of Birth":DOBOFAC,
              "Nationality":countryMapping(countryList[0], replaceUN(nationalityOFAC,true),false),
              "Gender":'OFAC',
              "Country of Residence":countryMapping(countryList[0], replaceUN(nationalityOFAC,true),false),
              "ID Type":nationalIDOFAC ? 'National ID Card' : passportOFAC? 'Passport':'',
              "ID Number":nationalIDOFAC ? nationalIDOFAC : passportOFAC ? passportOFAC :''
            };
            jsonArr.push(jsonAlias);
          }
        }
      }

      excel(jsonArr);
    }



    function isNumeric(num){
      return !isNaN(num)
    }

    function countryMapping(countryList,countryName,isUN){

      var country ='';

      loop1:
        for(let i=0; i < countryList.length; i++){
          if(isUN){
            // Mapping For UN
            // Getting values from UN column
            let b = countryList[i].UN.trim();
            let words = b.split(';');
            // console.log(words);
            loop2:
              for(let l=0; l< words.length;l++){
                if(words[l].trim().toLowerCase() == countryName.trim().toLowerCase()){
                  country = countryList[i].Country_Title;
                  // console.log("UN "+country);
                  break loop1;
                }else{
                  country = countryName;
                  // console.log("UN 2 "+country);
                }
              }
          }else{
            // Mapping For OFAC
            // Getting values from OFAC column
            // let words = countryList[i].OFAC.split(';');
            let a = countryList[i].OFAC.trim();
            let words = a.split(';');
            loop3:
              for(let k=0; k< words.length;k++){
                if(words[k].trim().toLowerCase() == countryName.trim().toLowerCase()){
                  country = countryList[i].Country_Title;
                  // console.log("OFAC "+country+" Check "+words[k].trim().toLowerCase());
                  break loop1;
                }else{
                  country = countryName;
                  // console.log("2 "+country);
                }
              }
          }
        }
      // // console.log(country);
      return country;

    }

    function formatDate(dateStr,format){
      let formatedDate = '';
      let count = '';
      count = (dateStr.match(new RegExp("-", "g")) || []).length;

      if(count > 2){
        dateStr = dateStr.substring(10,0);
      }

      if(dateStr.length < 4){
        if(dateStr.length == 3){
          dateStr = dateStr+"0";
        }else if(dateStr == 2){
          dateStr = dateStr+"00";
        }else if(dateStr.length == 1){
          dateStr = dateStr+"000";
        }
      }

      try {
        if(dateStr){
          let date = new Date(dateStr);
          // Basic usage
          formatedDate = dateFormat(date,format);
        }
      } catch (error) {
        formatedDate = dateStr;
      }

      return formatedDate;
    }

    function replacePassport(str,isIndividual){
      let aa = str.split('@')[0];
      let bb = aa.split(' born')[0];
      let cc = bb.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '').trim();
      let a = cc.split(';')[0];
      let b = a.split('s/o')[0];
      let c = b.split('d/o')[0];
      let d = c.split('r/o')[0];
      let e = d.split('Caste')[0];

      let f = '';
      let g = '';
      let h = '';

      if(e.indexOf(')') == 0 || e.indexOf('(') == 0 ){

        f = e.replace('(','');
        h = f.replace(')','');
      }else{

        f = e.split('(')[0];
        h = f.split(')')[0];
      }


      // let i = h.split('/')[0];
      let i = h.replace('#','');

      let j = i.split('Alias')[0];
      let k = j.replace(/-/g,' ');
      // let l = k.replace(/\./g,'');
      let m = k.split('caste')[0];
      let n = m.split('alias')[0];
      let o = n.replace('Ch ','');
      let p = o.replace('Dr.','');
      let q = p.replace(/\./g,'');

      let r = '';
      if(isIndividual){
        r = q.replace(/,/g,'');
      }else{
        r = q.split(',')[0];
      }

      let s = r.split(' Aka ')[0];
      let t = s.split(' aka ')[0];
      let u = t.split(' aka ')[0];
      // let v = u.replace(/  /g,' ');
      let v = u.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '');
      let w = v.replace(/\'/g,'');
      let x = w.replace('Possibly','');
      let y = x.replace('possibly','');
      var z = y.replace(/\s+/g,' ').replace(/^\s+|\s+$/,'');
      var az = z.replace(':','')
      var bz = az.replace('Diplomatic Passport','')
      var cz = bz.split('expires')[0];
      var dz = cz.replace('Pakistani national identification number','')
      var ez = dz.replace('Diplomatic','').trim()
      var fz = ez.replace('Colombia','')
      var gz = fz.replace('Diplomatic Laissez Passer','')
      var hz = gz.replace('Stateless Person','')
      var iz = hz.replace('Libyan Passport No','')
      var jz = iz.replace('British number','')
      var kz = jz.replace('Provisional passport No','')
      var lz = kz.replace('Russian passport number','')
      var mz = lz.replace('Russian foreign travel passport number','')
      var nz = mz.replace('South Sudan','')
      var oz = nz.replace('Afghan passport number','')
      var pz = oz.replace('Indonesia travel document number','')
      var qz = pz.replace('Central African Republic number','')
      var rz = qz.replace('French national identity card number','')
      var sz = rz.replace('CAR diplomatic passport no','')
      var tz = sz.replace('French passport number','')
      var uz = tz.replace('Counterfeit Danish driving licence number','')
      var vz = uz.replace('Central African Republic armed forces','')
      var xz = vz.replace('Pakistani passport number','')
      var yz = xz.replace('national identity card number','')
      var zz = yz.replace('Laissezpasser no','') 
      var aaz = zz.replace('Libya','')
      var abz = aaz.replace('Afghan national identification card','')
      var acz = abz.replace('Yemen','')
      var adz = acz.replace('German travel document','')
      var aez = adz.replace('Tatmadaw Kyee','')
      var afz = aez.replace('Jordan','')
      var agz = afz.replace('Laissez Passer','')
      var ahz = agz.replace('No','')
      var aiz = ahz.replace('NO','')
      var ajz = aiz.replace('NIL','')
      var akz = ajz.replace('nil','')
      return akz.trim();
    }

    function replaceUN(str,isIndividual){
      let aa = str.split('@')[0];
      let bb = aa.split(' born')[0];
      let cc = bb.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '').trim();
      let a = cc.split(';')[0];
      let b = a.split('s/o')[0];
      let c = b.split('d/o')[0];
      let d = c.split('r/o')[0];
      let e = d.split('Caste')[0];

      let f = '';
      let g = '';
      let h = '';

      if(e.indexOf(')') == 0 || e.indexOf('(') == 0 ){

        f = e.replace('(','');
        h = f.replace(')','');
      }else{

        f = e.split('(')[0];
        h = f.split(')')[0];
      }


      let i = h.split('/')[0];
      let j = i.split('Alias')[0];
      let k = j.replace(/-/g,' ');
      // let l = k.replace(/\./g,'');
      let m = k.split('caste')[0];
      let n = m.split('alias')[0];
      let o = n.replace('Ch ','');
      let p = o.replace('Dr.','');
      let q = p.replace(/\./g,'');

      let r = '';
      if(isIndividual){
        r = q.replace(/,/g,'');
      }else{
        r = q.split(',')[0];
      }

      let s = r.split(' Aka ')[0];
      let t = s.split(' aka ')[0];
      let u = t.split(' aka ')[0];
      // let v = u.replace(/  /g,' ');
      let v = u.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '');
      let w = v.replace(/\'/g,'');
      let x = w.replace('Possibly','');
      let y = x.replace('possibly','');
      var z = y.replace(/\s+/g,' ').replace(/^\s+|\s+$/,'');
      var az = z.replace(':','')
      return az;

    }


    function replaceNACTA(str,isIndividual){
      
      let aa = str.split('@')[0];
      let bb = aa.split(' born')[0];
      let cc = bb.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '').trim();
      let a = cc.split(';')[0];
      let b = a.split('s/o ')[0];
      let bc = b.split('S/o ')[0];
      let ccc = bc.split('W/O ')[0];
      let ccd = ccc.split('w/o ')[0];
      let c = ccd.split('d/o')[0];
      let d = c.split('r/o')[0];
      let e = d.split('Caste')[0];

      let f = '';
      let g = '';
      let h = '';

      if(e.indexOf(')') == 0 || e.indexOf('(') == 0 ){

        f = e.replace('(','');
        h = f.replace(')','');
      }else{

        f = e.split('(')[0];
        h = f.split(')')[0];
      }


      let i = h.split('/')[0];
      let j = i.split('Alias')[0];
      let k = j.replace(/-/g,' ');
      // let l = k.replace(/\./g,'');
      let m = k.split('caste')[0];
      let n = m.split('alias')[0];
      let o = n.replace('Ch ','');
      let p = o.replace('Dr.','');
      let q = p.replace(/\./g,'');

      let r = '';
      if(isIndividual){
        r = q.replace(/,/g,'');
      }else{
        r = q.split(',')[0];
      }

      let s = r.split(' Aka ')[0];
      let t = s.split(' aka ')[0];
      let u = t.split(' aka ')[0];
      // let v = u.replace(/  /g,' ');
      let v = u.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '');
      let w = v.replace(/\'/g,'');
      let x = w.replace('Possibly','');
      let y = x.replace('possibly','');
      var z = y.replace(/\s+/g,' ').replace(/^\s+|\s+$/,'');
      let az = z.replace(/NIL/g,'')
      return az;

    }

    function excel(json) {
      var xls = json2xls(jsonArr);
      fs.writeFileSync('test-latest-watch-list-dec.xlsx', xls, 'binary');
    }

    function number(num){
      var returnVal = '';
      if(num.toString().length == 1){
        returnVal = "00000"+num;
      }else if(num.toString().length == 2){
        returnVal = "0000"+num;
      }else if(num.toString().length == 3){
        returnVal = "000"+num;
      }else if(num.toString().length == 4){
        returnVal = "00"+num;
      }else if(num.toString().length == 5){
        returnVal = "0"+num;
      }else {
        returnVal = num;
      }
      return returnVal;
    }

  },

  createCustomer: function (req, res) {

    // var countryList = parser.parseXls2Json('country_list_mapping.xlsx');
    // console.log("Final "+countryMapping(countryList[0],'Democratic Peoples Republic Of Korea',true));

    // console.log(formatedDate('','dd/mm/yyyy'));
    let words = '';

    let DOBOFAC = 'DOB 1965; nationality Egypt; Gender Male.';
    DOBOFAC = DOBOFAC.replace("DOB","").trim().replace(/alt/ig,"").trim().replace(/ /g, '-');
    DOBOFAC = DOBOFAC.replace(/circa/ig,"").trim();
    console.log(DOBOFAC);
    DOBOFAC = formatDate(DOBOFAC,"dd/mm/yyyy");
    console.log(DOBOFAC);


    function countryMapping(countryList,countryName,isUN){

      var country ='';

      loop1:
        for(let i=0; i < countryList.length; i++){
          if(isUN){
            // Mapping For UN
            // Getting values from UN column
            let a = countryList[i].UN.trim();
            let words = a.split(';');
            console.log(words);
            loop2:
              for(let k=0; k< words.length;k++){
                if(words[k].trim().toLowerCase() == countryName.trim().toLowerCase()){
                  country = countryList[i].Country_Title;
                  console.log("UN "+country);
                  break loop1;
                }else{
                  country = countryName;
                  console.log("UN 2 "+country);
                }
              }
          }else{
            // Mapping For OFAC
            // Getting values from OFAC column
            // let words = countryList[i].OFAC.split(';');
            let a = countryList[i].OFAC.trim();
            let words = a.split(';');
            console.log(words);
            loop3:
              for(let k=0; k< words.length;k++){
                if(words[k].trim().toLowerCase() == countryName.trim().toLowerCase()){
                  country = countryList[i].Country_Title;
                  console.log("OFAC "+country);
                  break loop1;
                }else{
                  country = countryName;
                  console.log("2 "+country);
                }
              }
          }
        }
      // // console.log(country);
      return country;

    }

    function formatDate(dateStr,format){
      let formatedDate = '';
      let count = '';
      count = (dateStr.match(new RegExp("-", "g")) || []).length;

      if(count > 2){
        dateStr = dateStr.substring(10,0);
      }

      if(dateStr.length < 4){
        if(dateStr.length == 3){
          dateStr = dateStr+"0";
        }else if(dateStr == 2){
          dateStr = dateStr+"00";
        }else if(dateStr.length == 1){
          dateStr = dateStr+"000";
        }
      }

      try {
        if(dateStr){
          let date = new Date(dateStr);
          // Basic usage
          formatedDate = dateFormat(date,format);
        }
      } catch (error) {
        formatedDate = dateStr;
      }

      return formatedDate;

    }


    function replaceUN(str,isIndividual){
      let aa = str.split('@')[0];
      let bb = aa.split(' born')[0];
      let cc = bb.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '').trim();
      let a = cc.split(';')[0];

      let b = a.split('s/o')[0];
      let c = b.split('d/o')[0];
      let d = c.split('r/o')[0];
      let e = d.split('Caste')[0];

      let f = '';
      let g = '';
      let h = '';

      if(e.indexOf(')') == 0 || e.indexOf('(') == 0 ){

        f = e.replace('(','');
        h = f.replace(')','');
      }else{

        f = e.split('(')[0];
        h = f.split(')')[0];
      }



      let i = h.split('/')[0];
      let j = i.split('Alias')[0];
      let k = j.replace(/-/g,' ');
      // let l = k.replace(/\./g,'');
      let m = k.split('caste')[0];
      let n = m.split('alias')[0];
      let o = n.replace('Ch ','');
      let p = o.replace('Dr.','');
      let q = p.replace(/\./g,'');

      let r = '';
      if(isIndividual){
        r = q.replace(/,/g,'');
      }else{
        r = q.split(',')[0];
      }

      let s = r.split(' Aka ')[0];
      let t = s.split(' aka ')[0];
      let u = t.split(' aka ')[0];
      // let v = u.replace(/  /g,' ');
      // let v = u.replace(/[\x00-\x08\x0E-\x1F\x7F-\uFFFF]/g, '');
      let w = u.replace(/\'/g,'');
      let x = w.replace('Possibly','');
      let y = x.replace('possibly','');
      var z = y.replace(/\s+/g,' ').replace(/^\s+|\s+$/,'');
      return z;

    }




  },

  testCallBack: function (req, res) {

    // customer.Test(req,res,function(result){
    //     console.log("Controller "+result);
    //     res.status(200).json(result);
    // });
  }
};

module.exports = controller;